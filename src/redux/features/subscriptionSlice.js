import { createSlice } from "@reduxjs/toolkit";

export const subscriptionSlice = createSlice({
  name: "subscription",
  initialState: {
    subscribed: false,
  },
  reducers: {
    getSubscribed: (state) => {
      state.subscribed = true;
    },
    getUnsubscribed: (state) => {
      state.subscribed = false;
    },
  },
});

export const { getSubscribed, getUnsubscribed } = subscriptionSlice.actions;
export default subscriptionSlice.reducer;
