import { createSlice } from "@reduxjs/toolkit";

export const userEmailSlice = createSlice({
  name: "userEmail",
  initialState: {
    email: "",
  },
  reducers: {
    setUserEmail: (state, action) => {
      state.email = action.payload;
    },
  },
});

export const { setUserEmail } = userEmailSlice.actions;
export default userEmailSlice.reducer;
