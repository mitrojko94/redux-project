import { configureStore } from "@reduxjs/toolkit";
import visibilityReducer from "./features/visibilitySlice";
import subscriptionReducer from "./features/subscriptionSlice";
import userEmailReducer from "./features/userEmailSlice";

const store = configureStore({
  reducer: {
    visibility: visibilityReducer,
    subscription: subscriptionReducer,
    userEmail: userEmailReducer,
  },
});

export default store;
