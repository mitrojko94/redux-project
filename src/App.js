// App.js
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getSubscribed,
  getUnsubscribed,
} from "./redux/features/subscriptionSlice";
import { hide, show } from "./redux/features/visibilitySlice";
import { setUserEmail } from "./redux/features/userEmailSlice";
import "./index.css";

const App = () => {
  const [communityData, setCommunityData] = useState([]);
  const [subscribeLoading, setSubscribeLoading] = useState(false);
  const [unsubscribeLoading, setUnsubscribeLoading] = useState(false);
  const [sectionVisible, setSectionVisible] = useState(true);

  // Getting values from redux store
  const dispatch = useDispatch();
  const isVisible = useSelector((state) => state.visibility.visible);
  const isSubscribed = useSelector((state) => state.subscription.subscribed);
  const userEmail = useSelector((state) => state.userEmail.email);

  const handleSubmit = (e) => {
    e.preventDefault();
    isSubscribed ? unsubscribe() : subscribe();
  };

  const handleClick = () => {
    isVisible ? dispatch(hide()) : dispatch(show());
  };

  useEffect(() => {
    fetchCommunityData();
  }, []);

  const fetchCommunityData = async () => {
    try {
      const response = await fetch("http://localhost:8000/community");
      const data = await response.json();
      setCommunityData(data);
    } catch (error) {
      console.error("Error fetching community data:", error);
    }
  };
  // SUBSCRIBE
  const subscribe = () => {
    try {
      fetch("http://localhost:8000/subscribe", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email: userEmail }),
      })
        .then((response) => response.json())
        .then((response) =>
          response.error ? alert(response.error) : dispatch(getSubscribed())
        )
        .catch((error) => alert(error));
      setUserEmail("");
    } catch (error) {
      alert(error);
      setUserEmail("");
    }
  };

  // UNSUBSCRIBE
  const unsubscribe = () => {
    try {
      fetch("http://localhost:8000/unsubscribe", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email: userEmail }),
      })
        .then(dispatch(getUnsubscribed()))
        .catch((error) => alert(error));
      setUserEmail("");
    } catch (error) {
      alert(error);
    }
  };

  const toggleSectionVisibility = () => {
    setSectionVisible(!sectionVisible);
  };

  return (
    <div>
      <section>
        <div className="community">
          <h2>
            Big Community of <br /> People Like You
          </h2>

          <div onClick={toggleSectionVisibility}>
            <button type="button" onClick={handleClick}>
              {sectionVisible ? "Hide section" : "Show section"}
            </button>
          </div>
        </div>

        <h2 className="title">
          We're proud of our products, and we're really excited <br /> when we
          get feedback from our users.
        </h2>

        <div className="container">
          {communityData.map((person) => {
            return (
              <div key={person.id} className="personData">
                <img
                  src={person.avatar}
                  alt={person.firstName}
                  className="pImage"
                />
                <p className="pData">
                  Lorem ipsum dolor sit amet, <br />
                  consectetur adipiscing elit, sed do <br />
                  eiusmod tempor incididunt ut <br />
                  labore et dolor.
                </p>
                <h1 className="pName">
                  {person.firstName} {person.lastName}
                </h1>
                <p className="pPosition">{person.position}</p>
              </div>
            );
          })}
        </div>
      </section>

      <section style={{ opacity: sectionVisible ? 1 : 0 }}>
        <form className="form" onSubmit={handleSubmit}>
          <h2>Join Our Program</h2>

          <input
            type="email"
            placeholder="Enter your email"
            value={userEmail}
            onChange={(e) => dispatch(setUserEmail(e.target.value))}
          />

          <button
            type="button"
            onClick={subscribe}
            disabled={subscribeLoading || unsubscribeLoading}
            style={{
              opacity: subscribeLoading || unsubscribeLoading ? 0.5 : 1,
            }}
          >
            Subscribe
          </button>
          <button
            type="button"
            onClick={unsubscribe}
            disabled={subscribeLoading || unsubscribeLoading}
            style={{
              opacity: subscribeLoading || unsubscribeLoading ? 0.5 : 1,
            }}
          >
            Unsubscribe
          </button>
        </form>
      </section>
    </div>
  );
};

export default App;
